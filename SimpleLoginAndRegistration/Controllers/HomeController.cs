﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SimpleLoginAndRegistration.Models;
using static DataLibrary.Queries;
//using Dapper;

namespace SimpleLoginAndRegistration.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult ReverseNumber(ReverseNumber model)
        {
            int Reverse = 0;
            try
            {
                if (ModelState.IsValid)
                {
                   
                    while (model.Numbers > 0)
                    {
                        Reverse = (Reverse * 10) + (model.Numbers % 10);
                        model.Numbers = model.Numbers / 10;
                    }
                }
            }
            catch(Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
            TempData["Numbers"] = Reverse;
            return RedirectToAction("ReverseNumber");
        }

        public ActionResult ReverseNumber()
        {
            return View();
        }

        public ActionResult Delete(int Id)
        {
            try
            {
                int d = DeleteRecords(Id);
                TempData["Results"] = "Deleted!";
                return RedirectToAction("ViewRecords");
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }

        [HttpPost]
        public ActionResult Update(ViewVolunteer model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int result = UpdateRecords(model.ID, model.FirstName, model.LastName, model.Age, model.BirthDay, model.Name, model.RegionName);
                }
                TempData["Results"] = "Updated!";
                return RedirectToAction("ViewRecords");
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
        
        [HttpGet]
        public ActionResult Edit(int Id)
        {
            List<ViewVolunteer> _v = new List<ViewVolunteer>();
            DataSet ds = fetchAll();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    _v.Add(new ViewVolunteer
                    {
                        ID = int.Parse(dr[0].ToString()),
                        FirstName = dr[1].ToString(),
                        LastName = dr[2].ToString(),
                        Age = int.Parse(dr[3].ToString()),
                        BirthDay = DateTime.Parse(dr[4].ToString()),
                        RegionName = dr[5].ToString(),
                        Name = dr[6].ToString()
                    });
                }
            }
            return View(_v.Where(x => x.ID == Id).FirstOrDefault());
        }

        public ActionResult ViewRecords()
        {
            List<ViewVolunteer> _v = new List<ViewVolunteer>();
            DataSet ds = fetchAll();
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    _v.Add(new ViewVolunteer
                    {
                        ID = int.Parse(dr[0].ToString()),
                        FirstName = dr[1].ToString(),
                        LastName = dr[2].ToString(),
                        Age = int.Parse(dr[3].ToString()),
                        BirthDay = DateTime.Parse(dr[4].ToString()),
                        RegionName = dr[5].ToString(),
                        Name = dr[6].ToString()
                    });
                }
            }
            return View(_v);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(CreateVolunteer model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    int result = AddVolunteer(model.FirstName, model.LastName, model.Age, model.BirthDay, model.Name, model.RegionName);
                }
                ViewBag.Results = "Saved!"; 
                return View();
            }
            catch (Exception ex)
            {
                return Json("Error: " + ex.Message);
            }
        }
    }
}