﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleLoginAndRegistration.Models
{
    public class ReverseNumber
    {
        [Required(ErrorMessage = "This fields is required!")]
        public int Numbers { get; set; }
    }
}