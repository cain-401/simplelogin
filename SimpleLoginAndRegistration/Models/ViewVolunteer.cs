﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleLoginAndRegistration.Models
{
    public class ViewVolunteer
    {
        [Key]
        public int ID { get; set; }
        [Required(ErrorMessage = "FirstName is Required!")]
        public string FirstName { get; set; }
        [Required(ErrorMessage = "LastName is Required!")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Age is Required!")]
        public int Age { get; set; }
        [Required(ErrorMessage = "Birthday is Required!")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime BirthDay { get; set; }
        [Required(ErrorMessage = "Sex name is Required!")]
        [Display(Name="Sex")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Region is Required!")]
        public string RegionName { get; set; }
    }
}