﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleLoginAndRegistration.Models
{
    public class Volunteer
    {
        public int VolunteerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public DateTime BirthDay { get; set; }
        public int SexId { get; set; }
        public int RegionCode { get; set; }
    }
}