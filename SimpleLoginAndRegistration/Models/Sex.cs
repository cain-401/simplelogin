﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleLoginAndRegistration.Models
{
    public class Sex
    {
        public int SexId { get; set; }
        public string Name { get; set; }
    }
}