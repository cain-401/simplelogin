﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleLoginAndRegistration.Models
{
    public class Regions
    {
        public int RegionCode { get; set; }
        public string RegionName { get; set; }
    }
}