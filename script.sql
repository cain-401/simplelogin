USE [master]
GO
/****** Object:  Database [DBTest]    Script Date: 08/05/2021 2:20:34 AM ******/
CREATE DATABASE [DBTest]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'DBTest', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DBTest.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'DBTest_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\DBTest_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [DBTest] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [DBTest].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [DBTest] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [DBTest] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [DBTest] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [DBTest] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [DBTest] SET ARITHABORT OFF 
GO
ALTER DATABASE [DBTest] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [DBTest] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [DBTest] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [DBTest] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [DBTest] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [DBTest] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [DBTest] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [DBTest] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [DBTest] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [DBTest] SET  ENABLE_BROKER 
GO
ALTER DATABASE [DBTest] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [DBTest] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [DBTest] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [DBTest] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [DBTest] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [DBTest] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [DBTest] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [DBTest] SET RECOVERY FULL 
GO
ALTER DATABASE [DBTest] SET  MULTI_USER 
GO
ALTER DATABASE [DBTest] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [DBTest] SET DB_CHAINING OFF 
GO
ALTER DATABASE [DBTest] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [DBTest] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [DBTest] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'DBTest', N'ON'
GO
ALTER DATABASE [DBTest] SET QUERY_STORE = OFF
GO
USE [DBTest]
GO
/****** Object:  Table [dbo].[Regions]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Regions](
	[RegionCode] [int] IDENTITY(1,1) NOT NULL,
	[RegionName] [varchar](200) NULL,
PRIMARY KEY CLUSTERED 
(
	[RegionCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SexTable]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SexTable](
	[SexId] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[SexId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Volunteer]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Volunteer](
	[VolunteerId] [int] IDENTITY(1,1) NOT NULL,
	[FirstName] [varchar](100) NULL,
	[LastName] [varchar](100) NULL,
	[Age] [int] NULL,
	[BirthDay] [datetime] NULL,
	[SexId] [int] NULL,
	[RegionCode] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[VolunteerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Volunteer]  WITH CHECK ADD FOREIGN KEY([RegionCode])
REFERENCES [dbo].[Regions] ([RegionCode])
GO
ALTER TABLE [dbo].[Volunteer]  WITH CHECK ADD FOREIGN KEY([SexId])
REFERENCES [dbo].[SexTable] ([SexId])
GO
/****** Object:  StoredProcedure [dbo].[DeleteRecord]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteRecord]
	@id int
as
	delete from Volunteer where VolunteerId = @id
GO
/****** Object:  StoredProcedure [dbo].[FetchAll]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[FetchAll]
as
	select v.VolunteerId, v.FirstName,
		v.LastName, v.Age, v.BirthDay,
		r.RegionName, s.Name
	from Volunteer v
	left join Regions r on v.RegionCode = r.RegionCode
	left join SexTable s on v.SexId = s.SexId
GO
/****** Object:  StoredProcedure [dbo].[InSertVolunteer]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[InSertVolunteer]
	@FirstName varchar(100),
    @LastName varchar(100),
    @Age int,
    @BirthDay datetime,
    @Name varchar(100),
    @RegionName varchar(200)
as
	declare @sexId  int
	declare @regionId int

	set @sexId = (select SexId from SexTable where Name = @Name)

	if not exists (select SexId from SexTable where Name = @Name)
	begin
		insert into SexTable (Name) values(@Name)
		set @sexId =  IDENT_CURRENT('SexTable')
	end

	set @regionId = (select RegionCode from Regions where RegionName = @RegionName)

	if not exists (select RegionCode from Regions where RegionName = @RegionName)
	begin
		insert into Regions (RegionName) values (@RegionName)
		set @regionId = IDENT_CURRENT('Regions')
	end

	insert into Volunteer (FirstName, LastName, Age, BirthDay, SexId, RegionCode)
	values (@FirstName, @LastName, @Age, @BirthDay, @sexId, @regionId)
GO
/****** Object:  StoredProcedure [dbo].[UpdateRecords]    Script Date: 08/05/2021 2:20:35 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[UpdateRecords]
	@Id int,
	@FirstName varchar(100),
    @LastName varchar(100),
    @Age int,
    @BirthDay datetime,
    @Name varchar(100),
    @RegionName varchar(200)
as
	declare @sexId  int
	declare @regionId int

	set @sexId = (select SexId from SexTable where Name = @Name)

	if not exists (select SexId from SexTable where Name = @Name)
	begin
		insert into SexTable (Name) values(@Name)
		set @sexId =  IDENT_CURRENT('SexTable')
	end

	set @regionId = (select RegionCode from Regions where RegionName = @RegionName)

	if not exists (select RegionCode from Regions where RegionName = @RegionName)
	begin
		insert into Regions (RegionName) values (@RegionName)
		set @regionId = IDENT_CURRENT('Regions')
	end

	update Volunteer set FirstName = @FirstName, LastName = @LastName, Age = @Age, BirthDay = @BirthDay, SexId = @sexId, RegionCode = @regionId
	where VolunteerId = @Id
GO
USE [master]
GO
ALTER DATABASE [DBTest] SET  READ_WRITE 
GO
