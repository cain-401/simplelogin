﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Data;

namespace DataLibrary
{
    public static class Queries
    {
        private static int response = 0;
        public static int AddVolunteer(string FirstName, string LastName, int Age, DateTime BirthDay, string Name, string RegionName)
        {
            response = (int)ResponseCode.SUCCESSFUL;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "InSertVolunteer";
                cmd.Parameters.AddWithValue("@FirstName", FirstName);
                cmd.Parameters.AddWithValue("@LastName", LastName);
                cmd.Parameters.AddWithValue("@Age", Age);
                cmd.Parameters.AddWithValue("@BirthDay", BirthDay);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@RegionName", RegionName);
                BL.UpdateDate(cmd);
            }
            catch(Exception ex)
            {
                response = (int)ResponseCode.ERROR;
            }
            return response;
        }

        public static DataSet fetchAll()
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "FetchAll";
            DataSet ds = BL.GetData(cmd);
            return ds;
        }

        public static int DeleteRecords(int id)
        {
            response = (int)ResponseCode.SUCCESSFUL;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "DeleteRecord";
                cmd.Parameters.AddWithValue("@id", id);
                BL.UpdateDate(cmd);
            }
            catch (Exception ex)
            {
                response = (int)ResponseCode.ERROR;
            }
            return response;
        } 
        
        public static int UpdateRecords(int Id, string FirstName, string LastName, int Age, DateTime BirthDay, string Name, string RegionName)
        {
            response = (int)ResponseCode.SUCCESSFUL;
            try
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "UpdateRecords";
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@FirstName", FirstName);
                cmd.Parameters.AddWithValue("@LastName", LastName);
                cmd.Parameters.AddWithValue("@Age", Age);
                cmd.Parameters.AddWithValue("@BirthDay", BirthDay);
                cmd.Parameters.AddWithValue("@Name", Name);
                cmd.Parameters.AddWithValue("@RegionName", RegionName);
                BL.UpdateDate(cmd);
            }
            catch(Exception ex)
            {
                response = (int)ResponseCode.ERROR;
            }
            return response;
        }
    }
}
