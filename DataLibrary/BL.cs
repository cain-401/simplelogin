﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataLibrary
{
    public class BL
    {
        public static DataSet GetData(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection();
            DataSet dt = new DataSet();
            try
            {
                cmd.Connection = Connection.connectToDB(con);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter ad = new SqlDataAdapter();
                ad.SelectCommand = cmd;
                ad.Fill(dt);
            }
            catch (Exception)
            {
                throw;
            }
            return dt;
        }

        public static void UpdateDate(SqlCommand cmd)
        {
            SqlConnection con = new SqlConnection();
            try
            {
                cmd.Connection = Connection.connectToDB(con);
                cmd.ExecuteNonQuery();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                Connection.closeConnection(con);
            }
        }
    }
}
