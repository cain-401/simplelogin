﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
//using Dapper;
using System.Text;
using System.Linq;

namespace DataLibrary
{
    public class Connection
    {
        public static string GetConnectionString(string connection = "mscon")
        {
            return ConfigurationManager.ConnectionStrings[connection].ConnectionString;
        }

        public static SqlConnection connectToDB(SqlConnection con)
        {
            con.ConnectionString = GetConnectionString();
            con.Open();
            return con;
        }
        
        public static void closeConnection(SqlConnection con)
        {
            if(con.State == ConnectionState.Open)
            {
                con.Close();
            }
        }
    }
}
