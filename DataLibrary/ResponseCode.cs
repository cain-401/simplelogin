﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DataLibrary
{
   public enum ResponseCode
    {
        SUCCESSFUL = 1000,
        ERROR = 9999,
        NO_RECORD = 3000
    }
}
